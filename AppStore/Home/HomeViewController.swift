//
//  HomeViewController.swift
//  AppStore
//
//  Created by mac on 26/03/20.
//  Copyright © 2020 luv.mac. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase
import FirebaseDatabase
import FirebaseStorage
import SafariServices




class homeCollectionViewCell :UITableViewCell
{
    @IBOutlet weak var lbl_day: UILabel!
    
    @IBOutlet weak var lbl_date: UILabel!
    
    @IBOutlet weak var img_post: CustomImageView!
    @IBOutlet weak var lbl_title: UILabel!
    
    @IBOutlet weak var lbl_ummary: UILabel!
    
    @IBOutlet weak var lbl_postedBy: UILabel!
    
}

class HomeViewController: UIViewController {
    var ref: DatabaseReference!
    var databasehandle:DatabaseHandle!
    
    var array_post :NSMutableArray = []
    var dic_post: NSMutableDictionary = [:]
    
   
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        self.showPostList();
        self.rightRefreshButton()

    }
    
    func rightRefreshButton()
    {
        let frameimg = CGRect(x: 0, y: 0, width: 30, height: 30)
        let someButton = UIButton(frame: frameimg)
        someButton.setImage(UIImage(named: "refresh"), for: .normal)
        
        someButton.backgroundColor = UIColor.blue
        someButton.cornerRadius = 15
        someButton.addTarget(self, action: #selector(self.refreshButtonAction), for: .touchUpInside)
        
        let mailbutton = UIBarButtonItem(customView: someButton)
        navigationItem.rightBarButtonItem = mailbutton
        
        let heightConstraint = NSLayoutConstraint(item: someButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: frameimg.height)
        let widthConstraint = NSLayoutConstraint(item: someButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: frameimg.width)
        heightConstraint.isActive = true;
        widthConstraint.isActive = true;
        
    }
    
    @objc func refreshButtonAction(_ sender: Any)
    {

        self.showPostList()
    }
    
   func showPostList()
    {
        ref = Database.database().reference()

        databasehandle = ref?.child("post").queryOrdered(byChild: "timestamp").observe(.value, with: { (snapshot) in
            
            self.array_post.removeAllObjects()
            
            for rest in snapshot.children.allObjects as! [DataSnapshot]
            {
                let temp_message = rest.value as! NSDictionary
                
                self.array_post.add(temp_message)
            }
            
            self.array_post = NSMutableArray(array: self.array_post.reversed())
            self.tableView.reloadData()


    })
        
       
    }

   
}


extension HomeViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.array_post.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "homeCollectionViewCell"

        
       guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? homeCollectionViewCell  else {
           fatalError("The dequeued cell is not an instance of ToDoHeaderTableCell.")
       }
       
       cell.selectionStyle = .none
        

         let post : NSDictionary = self.array_post[indexPath.row] as! NSDictionary
           
        if let day :String = (post.object(forKey: "date")as? String) {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.timeZone = TimeZone.current
            let utcDate = Date().toGlobalTime()
            let localDate = utcDate.toLocalTime()
            
            let date = dateFormatter.date(from: day)
            
            
            cell.lbl_day.text = self.timeAgoSinceDate(date!, currentDate: localDate, numericDates: true)
        }
        
         if let date :String = (post.object(forKey: "date")as? String)
         {
            cell.lbl_date.text = "{" + date + "}"
         }
        
        
        
        if let title :String = (post.object(forKey: "title")as? String)
        {
            cell.lbl_title.text = title
        }
            
            
            
        if let summary :String = (post.object(forKey: "summary")as? String)
       {
           cell.lbl_ummary.text = summary
       }
            
          
            
        if let posted :String = (post.object(forKey: "posted by")as? String)
            {
             cell.lbl_postedBy.text = "Posted by" + " " + posted
            }
            
            cell.lbl_postedBy.text = "Posted by" + " admin"
           
            
        if let image :String = post.object(forKey:"image")as? String
        {
            cell.img_post.contentMode = .scaleAspectFit
            cell.img_post.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "placeholder.png"))
            


        }
            
           
            
            return cell
    }
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let detail : NSDictionary = self.array_post.object(at: indexPath.row) as! NSDictionary
        
//        let safariVC = SFSafariViewController(url: URL(string: "\(detail.object(forKey: "URL") ?? "www.technotoil.com")")!)
//        self.present(safariVC, animated: true, completion: nil)
        
        let url :String = detail.object(forKey: "URL") as! String
         let Obj_WebViewDetailViewController  = storyboard!.instantiateViewController(withIdentifier: "WebViewDetailViewController") as! WebViewDetailViewController
        
        Obj_WebViewDetailViewController.url = url
        
       self.navigationController?.pushViewController(Obj_WebViewDetailViewController, animated: true)
        
    }
   
}


    
    

