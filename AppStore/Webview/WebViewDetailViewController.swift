//
//  WebViewDetailViewController.swift
//  AppStore
//
//  Created by mac on 14/04/20.
//  Copyright © 2020 luv.mac. All rights reserved.
//

import UIKit
import WebKit

class WebViewDetailViewController: UIViewController,WKNavigationDelegate,WKUIDelegate{
   
    @IBOutlet weak var progress_view: UIProgressView!
    @IBOutlet weak var webview: WKWebView!
    var url :String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
       let url_NSURL = NSURL(string: url)
        let request = NSURLRequest(url: url_NSURL! as URL)
        webview.load(request as URLRequest)
        webview.uiDelegate = self
        
       
        progress_view.sizeToFit()
        progress_view.progress = 0.0
        
        self.webview.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil);
        
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            print(self.webview.estimatedProgress);
            progress_view.isHidden = webview.estimatedProgress == 1
            self.progress_view.progress = Float(self.webview.estimatedProgress);
        }
        
        
    }

    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.progress_view.setProgress(0.1, animated: false)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.progress_view.setProgress(1.0, animated: true)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.progress_view.setProgress(1.0, animated: true)
    }
}
